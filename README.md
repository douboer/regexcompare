
# regular express vs. xpath vs. beatifulsoup

python里涉及log分析，html分析等，几个常用库:

- regular express通用灵活, 自带库，打包方便、便携
- xpath和beatifulsoup使用相对简单，并提供了丰富库函数，入门容易, 满足xml、html等解析需要。

个人从c和perl过来，最爱正则。
因对正则最熟悉，之前项目全用正则解析，正则可以满足一切需要。

python中，可能是因为那句所谓人生苦短的箴言的撩拨^_^，方便的xpath & bs4在项目中广泛使用。

那么与regex比较，性能如何呢？对比纯粹出于好奇。
正好借之前写[kindle阅读管理工具](https://gitee.com/douboer/kman)里豆瓣信息管理部分，对三个方法做一个比较。

## 方法
- 从豆瓣读书pa出来的数据，格式是html
- 数据大小，4096*n行 & 1009440行

## 代码[详见](tcompare.py)

### regular express 方法

几个正则
```python
        re_bn=re.compile(r'''class=\"nbg.+?sid: (\d+?),.+?title=\"(.+?)\".+?img src=\"(.+?)\"''')
        re_star=re.compile(r'''^<span class=\"allstar(\d+)\"></span>''')
        re_score=re.compile(r'''class=\"rating_nums\">(.+?)<''')
        re_ratenum=re.compile(r'''^<span>\((\d+)人评价\)</span>''')
        re_author=re.compile(r'''class=\"subject-cast\">(.+?)<''')
        re_description=re.compile(r'''^<p>(.+?)(<\/p>){0,1}$''')
        re_sid=re.compile('^.+sid: (\d+),.+$')
```

```python
    def grab_book_info_regex(self, resp):

        # 状态机遍历
        [sid, stat, idx] = [None, 'SID', 0]

        for line in resp.split('\n'):
            line=line.strip()
            if line=='': continue

            if stat=='SID':
                ret=re.search(self.re_bn, line)
                if ret:
                    sid=ret.group(1)+'-{}'.format(idx)
                    bkinfo[sid]['link']=os.path.join(LINKPREF,sid)
                    bkinfo[sid]['bookname']=ret.group(2)
                    bkinfo[sid]['img']=ret.group(3)
                    idx += 1
                    stat='STAR'
                continue
            elif stat=='STAR':
                ret=re.search(self.re_star, line)
                if ret:
                    star = ret.group(1)
                    if star=='00':
                        stat='AUTHOR'
                    elif int(star) > 0:
                        stat='SCORE'
            elif stat=='SCORE':
                ret=re.search(self.re_score, line)
                if ret:
                    bkinfo[sid]['score']=ret.group(1)
                    stat='RATENUM'
                continue
            elif stat=='RATENUM':
                ret=re.search(self.re_ratenum, line)
                if ret:
                    bkinfo[sid]['ratenum']=ret.group(1)
                    stat='AUTHOR'
                continue
            elif stat=='AUTHOR':
                ret=re.search(self.re_author, line)
                if ret:
                    tt=ret.group(1).split(' / ')
                    if len(tt)>=3:
                        *author, bkinfo[sid]['publisher'], bkinfo[sid]['publishing']=tt
                        bkinfo[sid]['author']='/'.join(author)
                    else:
                        bkinfo[sid]['author']=ret[0]
                    stat='DESCRIPTION'
                continue
            elif stat=='DESCRIPTION':
                ret=re.search(self.re_description, line)
                if ret:
                    bkinfo[sid]['description']=ret.group(1)
                    stat='SID'
                continue
            else: continue

        return bkinfo
```

### bs4方法

```python
    def grab_book_info_bs4(self, resp):
        ...
        atag = soup.find_all("div", attrs={"class": "result"})
        ...
        for t in atag:
            # sp is type of bs4.element.Tag
            sp = t.span
        ...
            
        return bkinfo
```

### xpath方法

```python
    def grab_book_info_xpath(self, resp):
        dom = etree.HTML(resp)
        result = dom.xpath("//div[@class='result']")

        ...
        for s in result:
            ts = etree.tostring(s).decode('utf8')
        ...

        return bkinfo
```


### main 时间计算

```python
    [dic_count_x, dic_time_re, dic_time_xpath, dic_time_bs] = [[],[],[],[]]
    for n in range(5):
        _resp = '\n'.join([resp]*(n+1))
        linecount = lnum*(n+1)
        dic_count_x.append(linecount)

        start = time.time()
        bkinfo=spide.grab_book_info_regex(_resp)
        end = time.time()
        regex_cost = end-start

        start = time.time()
        bkinfo = spide.grab_book_info_xpath(_resp)
        end = time.time()
        dic_time_xpath.append(xpath_cost)

        start = time.time()
        bkinfo = spide.grab_book_info_bs4(_resp)
        end = time.time()
        bs4_cost = end-start
```

### 画曲线

```python
    plt.title('REGEX VS. XPATH VS. BEAUTIFULSOUP COMPARE (Chengan)')
    plt.xlabel('Line Numbers')
    plt.ylabel('Time Cost(seconds)')
    plt.plot(dic_count_x, dic_time_re, linestyle='-', marker='o',color='m',label='REGEX')
    plt.plot(dic_count_x, dic_time_xpath, linestyle='--', marker='*',color='g',label='XPATH')
    plt.plot(dic_count_x, dic_time_bs, linestyle=':', marker='.',color='c',label='BS4')
    plt.show()
```

## 结论

**速度 regex > xpath >> beautifulsoup**<br>
**regex = 6*xpath = 48*beautifulsoup**


**100万行**:<br>
- regex cost time - 3.1692097187042236 seconds
- beautifulsoap cost time - 984.9253339767456 seconds <br>
:bangbang: 310.78倍 


![compare](compare.png)


## 附表
记录数|方法|匹配长度|消耗时间(secends)
--|--|--|--
25235|regex|50|0.012480020523071289
25235|xpath|50|0.05919289588928223
25235|bs4|50|0.23454809188842773
50470|regex|100|0.026102066040039062
50470|xpath|100|0.11078596115112305
50470|bs4|100|0.5006749629974365
75705|regex|150|0.03772115707397461
75705|xpath|150|0.17281794548034668
75705|bs4|150|0.7439761161804199
100940|regex|200|0.052933692932128906
100940|xpath|200|0.2271900177001953
100940|bs4|200|1.0550971031188965
126175|regex|250|0.06651473045349121
126175|xpath|250|0.283886194229126
126175|bs4|250|1.3366880416870117
151410|regex|300|0.0787210464477539
151410|xpath|300|0.3446078300476074
151410|bs4|300|1.631767988204956
176645|regex|350|0.0939640998840332
176645|xpath|350|0.4012761116027832
176645|bs4|350|1.9854719638824463
201880|regex|400|0.10525202751159668
201880|xpath|400|0.4650130271911621
201880|bs4|400|2.3187568187713623
227115|regex|450|0.1171262264251709
227115|xpath|450|0.5236179828643799
227115|bs4|450|2.6632559299468994
252350|regex|500|0.13094806671142578
252350|xpath|500|0.5802679061889648
252350|bs4|500|3.042638063430786
277585|regex|550|0.14737725257873535
277585|xpath|550|0.6532280445098877
277585|bs4|550|3.425574779510498
302820|regex|600|0.1576519012451172
302820|xpath|600|0.7177207469940186
302820|bs4|600|3.8270480632781982
328055|regex|650|0.17104792594909668
328055|xpath|650|0.7991397380828857
328055|bs4|650|4.250217914581299
353290|regex|700|0.18520498275756836
353290|xpath|700|0.8482339382171631
353290|bs4|700|4.685260057449341
378525|regex|750|0.19573092460632324
378525|xpath|750|0.9133119583129883
378525|bs4|750|5.098837852478027
403760|regex|800|0.21107888221740723
403760|xpath|800|0.9814629554748535
403760|bs4|800|5.727763891220093
428995|regex|850|0.22483587265014648
428995|xpath|850|1.0595312118530273
428995|bs4|850|6.062335968017578
454230|regex|900|0.23551583290100098
454230|xpath|900|1.1162240505218506
454230|bs4|900|6.66562819480896
479465|regex|950|0.24890971183776855
479465|xpath|950|1.1943397521972656
479465|bs4|950|7.251280069351196
504700|regex|1000|0.2631049156188965
504700|xpath|1000|1.2687618732452393
504700|bs4|1000|7.834233045578003
529935|regex|1050|0.2752509117126465
529935|xpath|1050|1.3486049175262451
529935|bs4|1050|8.26619291305542
555170|regex|1100|0.2887430191040039
555170|xpath|1100|1.42454195022583
555170|bs4|1100|8.722611904144287
580405|regex|1150|0.3013780117034912
580405|xpath|1150|1.5220842361450195
580405|bs4|1150|9.565828800201416
605640|regex|1200|0.3206758499145508
605640|xpath|1200|1.5963201522827148
605640|bs4|1200|10.396812915802002
630875|regex|1250|0.3224821090698242
630875|xpath|1250|1.8285560607910156
630875|bs4|1250|11.185851097106934
656110|regex|1300|0.3410520553588867
656110|xpath|1300|1.7446479797363281
656110|bs4|1300|12.146087169647217
681345|regex|1350|0.3600189685821533
681345|xpath|1350|1.837920904159546
681345|bs4|1350|12.110383749008179
706580|regex|1400|0.3903679847717285
706580|xpath|1400|2.063889980316162
706580|bs4|1400|12.774359941482544
731815|regex|1450|0.3787820339202881
731815|xpath|1450|2.0633668899536133
731815|bs4|1450|13.17389178276062
757050|regex|1500|0.3900618553161621
757050|xpath|1500|2.101456880569458
757050|bs4|1500|14.675992012023926
782285|regex|1550|0.4209871292114258
782285|xpath|1550|2.3238730430603027
782285|bs4|1550|15.335577249526978
807520|regex|1600|0.5365860462188721
807520|xpath|1600|2.478172779083252
807520|bs4|1600|15.235244035720825
832755|regex|1650|0.4195220470428467
832755|xpath|1650|2.347764015197754
832755|bs4|1650|17.2506160736084
857990|regex|1700|0.4358210563659668
857990|xpath|1700|2.580536127090454
857990|bs4|1700|17.65079975128174
883225|regex|1750|0.44233202934265137
883225|xpath|1750|2.9625296592712402
883225|bs4|1750|19.122608184814453
908460|regex|1800|0.5809500217437744
908460|xpath|1800|2.8826301097869873
908460|bs4|1800|19.49199914932251
933695|regex|1850|0.5053651332855225
933695|xpath|1850|2.8267886638641357
933695|bs4|1850|20.691282033920288
958930|regex|1900|0.4949660301208496
958930|xpath|1900|2.8838658332824707
958930|bs4|1900|21.461628913879395
984165|regex|1950|0.49442195892333984
984165|xpath|1950|2.993624210357666
984165|bs4|1950|24.57726216316223
1009400|regex|2000|0.5195960998535156
1009400|xpath|2000|3.2465450763702393
1009400|bs4|2000|24.893566131591797

